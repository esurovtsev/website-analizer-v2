package com.grabduck.scout24test.services;

import com.grabduck.scout24test.models.AnalyzeResult;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class LoginFormParserTest {
    private LoginFormParser sut;

    @Before
    public void setUp() throws Exception {
        sut = new LoginFormParser();
    }

    @Test
    public void testParseWithLoginForm() throws Exception {
        Document doc = Jsoup.parse(
                "<!DOCTYPE html>" +
                        "<html lang=\"en\">" +
                        "<head>" +
                        "<meta charset=\"UTF-8\" />" +
                        "<title>Google</title>" +
                        "<meta name=\"description\" content=\"The latest entertainment news\" />" +
                        "<meta name=\"keywords\" content=\"hollywood gossip, hollywood news\" />" +
                        "</head>" +
                        "<body>" +
                        "<form>" +
                        "  <div class=\"form-group\">" +
                        "    <label for=\"exampleInputEmail1\">Email address</label>" +
                        "    <input type=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Email\">" +
                        "  </div>" +
                        "  <div class=\"form-group\">" +
                        "    <label for=\"exampleInputPassword1\">Password</label>" +
                        "    <input type=\"password\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"Password\">" +
                        "  </div>" +
                        "  <div class=\"form-group\">" +
                        "    <label for=\"exampleInputFile\">File input</label>" +
                        "    <input type=\"file\" id=\"exampleInputFile\">" +
                        "    <p class=\"help-block\">Example block-level help text here.</p>" +
                        "  </div>" +
                        "  <div class=\"checkbox\">" +
                        "    <label>" +
                        "      <input type=\"checkbox\"> Check me out" +
                        "    </label>" +
                        "  </div>" +
                        "  <button type=\"submit\" class=\"btn btn-default\">Submit</button>" +
                        "</form>" +
                        "</body>" +
                        "</html>");

        AnalyzeResult.AnalyzeResultBuilder builder = AnalyzeResult.builder();
        sut.parse(doc, builder);

        assertThat(builder.build().isContainsLoginForm(), is(true));
    }

    @Test
    public void testParseWithoutLoginForm() throws Exception {
        Document doc = Jsoup.parse(
                "<!DOCTYPE html>" +
                        "<html lang=\"en\">" +
                        "<head>" +
                        "<meta charset=\"UTF-8\" />" +
                        "<title>Google</title>" +
                        "<meta name=\"description\" content=\"The latest entertainment news\" />" +
                        "<meta name=\"keywords\" content=\"hollywood gossip, hollywood news\" />" +
                        "</head>" +
                        "<body>" +
                        "<div id='color'>This is red</div> />" +
                        "</body>" +
                        "</html>");

        AnalyzeResult.AnalyzeResultBuilder builder = AnalyzeResult.builder();
        sut.parse(doc, builder);

        assertThat(builder.build().isContainsLoginForm(), is(false));
    }
}