package com.grabduck.scout24test.services;

import com.grabduck.scout24test.models.UrlType;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class LinksParserTest {
    private LinksParser sut;

    @Before
    public void before() {
        sut = new LinksParser();
    }

    @Test
    public void testGetHostWithHttp() throws Exception {
        assertThat(sut.getHost("http://grabduck.com/last"), is("http://grabduck.com"));

    }

    @Test
    public void testGetHostWithHttps() throws Exception {
        assertThat(sut.getHost("https://grabduck.com/last"), is("https://grabduck.com"));

    }

    @Test
    public void testGetHostWithRoot() throws Exception {
        assertThat(sut.getHost("https://grabduck.com/"), is("https://grabduck.com"));

    }

    @Test
    public void testGetHostWithHash() throws Exception {
        assertThat(sut.getHost("https://grabduck.com#"), is("https://grabduck.com"));

    }

    @Test
    public void testGetHostWithHashAndRoot() throws Exception {
        assertThat(sut.getHost("https://grabduck.com/#"), is("https://grabduck.com"));
    }

    @Test
    public void testGetUrlTypeForInnacessible() {
        assertThat(sut.getUrlType("https://grabduck.com", ""), is(UrlType.INACCESSIBLE));
    }

    @Test
    public void testGetUrlTypeForExternal() {
        assertThat(sut.getUrlType("https://grabduck.com",
                "http://stackoverflow.com/questions/5045918/adding-a-share-by-email-link-to-website"),
                is(UrlType.EXTERNAL));
    }

    @Test
    public void testGetUrlTypeForInternal() {
        assertThat(sut.getUrlType("https://grabduck.com", "https://grabduck.com/last"), is(UrlType.INTERNAL));
    }
}