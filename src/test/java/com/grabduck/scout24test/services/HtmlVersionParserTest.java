package com.grabduck.scout24test.services;

import com.grabduck.scout24test.models.AnalyzeResult;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class HtmlVersionParserTest {
    private HtmlVersionParser sut;

    @Before
    public void setUp() throws Exception {
        sut = new HtmlVersionParser();
    }

    @Test
    public void testParse() throws Exception {
        Document doc = Jsoup.parse(
                "<!DOCTYPE html>" +
                        "<html lang=\"en\">" +
                        "<head>" +
                        "<meta charset=\"UTF-8\" />" +
                        "<title>Google</title>" +
                        "<meta name=\"description\" content=\"The latest entertainment news\" />" +
                        "<meta name=\"keywords\" content=\"hollywood gossip, hollywood news\" />" +
                        "</head>" +
                        "<body>" +
                        "<div id='color'>This is red</div> />" +
                        "</body>" +
                        "</html>");

        AnalyzeResult.AnalyzeResultBuilder builder = AnalyzeResult.builder();
        sut.parse(doc, builder);

        assertThat(builder.build().getHtmlVersion(), is("<!doctype html>"));
    }
}