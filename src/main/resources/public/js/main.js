angular.module("scout24Test", [])
    .controller("AppCtrl", function ($scope, $http) {
        function getPrintValue(value) {
            if (value === null) {
                return "n/a";
            } else {
                return value;
            }
        }

        $scope.showErrorMessage = false;
        $scope.inProcess = false;
        $scope.showResult = false;
        $scope.resultTable = [];

        $scope.sendUrl = function (url) {
            $scope.inProcess = true;
            $scope.showErrorMessage = false;
            $scope.showResult = false;

            $http({
                method: 'POST',
                url: "/analyze",
                data: {url: url}

            }).then(function (result) {
                var response = result.data;
                $scope.inProcess = false;
                $scope.showResult = true;
                $scope.resultTable = [{
                    title: "HTML version",
                    value: getPrintValue(response.htmlVersion)
                }, {
                    title: "Page title",
                    value: getPrintValue(response.pageTitle)
                }, {
                    title: "Heading level 1",
                    value: getPrintValue(response.h1)
                }, {
                    title: "Heading level 2",
                    value: getPrintValue(response.h2)
                }, {
                    title: "Heading level 3",
                    value: getPrintValue(response.h3)
                }, {
                    title: "Heading level 4",
                    value: getPrintValue(response.h4)
                }, {
                    title: "Heading level 5",
                    value: getPrintValue(response.h5)
                }, {
                    title: "Heading level 6",
                    value: getPrintValue(response.h6)
                }, {
                    title: "Internal links",
                    value: getPrintValue(response.internalLinks)
                }, {
                    title: "External links",
                    value: getPrintValue(response.externalLinks)
                }, {
                    title: "Inaccessible links",
                    value: getPrintValue(response.inaccessibleLinks)
                }, {
                    title: "Contains a login-form",
                    value: getPrintValue(response.containsLoginForm)
                }];

            }, function (error) {
                $scope.inProcess = false;
                $scope.showErrorMessage = true;
                $scope.errorMessage = error.data.message;
            });
        }
    });