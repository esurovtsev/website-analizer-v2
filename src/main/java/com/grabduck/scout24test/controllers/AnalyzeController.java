package com.grabduck.scout24test.controllers;

import com.grabduck.scout24test.dtos.ErrorResponseDto;
import com.grabduck.scout24test.dtos.UrlDto;
import com.grabduck.scout24test.models.AnalyzeResult;
import com.grabduck.scout24test.services.AnalyzerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.net.MalformedURLException;
import java.net.URL;

@RestController
public class AnalyzeController {
    @Autowired
    private AnalyzerService analyzerService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/analyze", method = RequestMethod.POST)
    public AnalyzeResult processUrl(@RequestBody UrlDto urlDto) {
        try {
            URL urlToProcess = new URL(urlDto.getUrl());
            return analyzerService.assembleWebsiteData(urlToProcess);

        } catch (MalformedURLException e) {
            throw new RuntimeException("The URL you provided is not correct!");
        }
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorResponseDto> handleRuntimeException(Exception ex) {
        ErrorResponseDto dto = new ErrorResponseDto();
        dto.setMessage(ex.getMessage());
        return new ResponseEntity<>(dto, HttpStatus.BAD_REQUEST);
    }
}
