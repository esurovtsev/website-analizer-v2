package com.grabduck.scout24test.utils;

public class Pair<L, R> {
    public final L _1;
    public final R _2;

    private Pair(L left, R right) {
        this._1 = left;
        this._2 = right;
    }

    public L getKey() { return _1; }
    public R getValue() { return _2; }

    @Override
    public int hashCode() { return _1.hashCode() ^ _2.hashCode(); }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Pair)) return false;
        Pair pairo = (Pair) o;
        return this._1.equals(pairo._1) &&
                this._2.equals(pairo._2);
    }

    public static <L, R> Pair<L, R> of(L left, R right) {
        return new Pair(left, right);
    }
}
