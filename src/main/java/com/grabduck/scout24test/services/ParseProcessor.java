package com.grabduck.scout24test.services;

import com.grabduck.scout24test.models.AnalyzeResult;
import org.jsoup.nodes.Document;

import java.util.Map;

public interface ParseProcessor {
    void parse(Document source, AnalyzeResult.AnalyzeResultBuilder resultBuilder);
}
