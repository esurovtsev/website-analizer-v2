package com.grabduck.scout24test.services;

import com.grabduck.scout24test.models.AnalyzeResult;
import lombok.NonNull;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.springframework.stereotype.Component;

@Component
public class HtmlVersionParser implements ParseProcessor {
    @Override
    public void parse(@NonNull Document source, @NonNull AnalyzeResult.AnalyzeResultBuilder resultBuilder) {
        source
                .childNodes()
                .stream()
                .filter(node -> node instanceof DocumentType)
                .findFirst()
                .map(node -> node.toString())
                .ifPresent(htmlVersion -> resultBuilder.htmlVersion(htmlVersion));
    }
}
