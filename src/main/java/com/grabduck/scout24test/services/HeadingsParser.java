package com.grabduck.scout24test.services;

import com.grabduck.scout24test.models.AnalyzeResult;
import lombok.NonNull;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

@Component
public class HeadingsParser implements ParseProcessor {
    @Override
    public void parse(@NonNull Document source, @NonNull AnalyzeResult.AnalyzeResultBuilder resultBuilder) {
        resultBuilder.h1(countHeaders(source, "h1"));
        resultBuilder.h2(countHeaders(source, "h2"));
        resultBuilder.h3(countHeaders(source, "h3"));
        resultBuilder.h4(countHeaders(source, "h4"));
        resultBuilder.h5(countHeaders(source, "h5"));
        resultBuilder.h6(countHeaders(source, "h6"));
    }

    private Integer countHeaders(@NonNull Document source, @NonNull String header) {
        Elements headings = source.select(header);
        return headings.size();
    }
}
