package com.grabduck.scout24test.services;

import com.google.common.annotations.VisibleForTesting;
import com.grabduck.scout24test.models.AnalyzeResult;
import com.grabduck.scout24test.models.UrlType;
import com.grabduck.scout24test.utils.Pair;
import lombok.NonNull;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toList;

@Component
public class LinksParser implements ParseProcessor {
    @Override
    public void parse(@NonNull Document source, @NonNull AnalyzeResult.AnalyzeResultBuilder resultBuilder) {
        final String host = getHost(source.location());

        List<Pair<String, UrlType>> urlsWithTypes = source
                .select("a[href]")
                .stream()
                .map(link -> link.absUrl("href"))
                .map(url -> Pair.of(url, getUrlType(host, url)))
                .collect(toList());

        resultBuilder.internalLinks(countUrlType(urlsWithTypes, entry -> entry.getValue() == UrlType.INTERNAL));
        resultBuilder.externalLinks(countUrlType(urlsWithTypes, entry -> entry.getValue() == UrlType.EXTERNAL));
        resultBuilder.inaccessibleLinks(countUrlType(urlsWithTypes, entry -> entry.getValue() == UrlType.INACCESSIBLE));
    }

    @VisibleForTesting
    UrlType getUrlType(@NonNull String host, @NonNull String url) {
        if (url.isEmpty()) {
            return UrlType.INACCESSIBLE;
        }
        return url.startsWith(host) ? UrlType.INTERNAL : UrlType.EXTERNAL;
    }

    private int countUrlType(@NonNull List<Pair<String, UrlType>> urlsWithTypes,
                             @NonNull Function<Pair<String, UrlType>, Boolean> predicateFn) {
        return (int) urlsWithTypes
                .stream()
                .filter(entry -> predicateFn.apply(entry))
                .count();
    }

    @VisibleForTesting
    String getHost(@NonNull String url) {
        Matcher m = HOST_PATTERN.matcher(url);
        m.matches();
        return m.group("host");
    }

    private final Pattern HOST_PATTERN = Pattern.compile("^(?<host>[a-zA-Z]*://.*?)(/|#).*");
}
