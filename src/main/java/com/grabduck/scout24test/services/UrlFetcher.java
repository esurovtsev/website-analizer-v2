package com.grabduck.scout24test.services;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.UnknownHostException;

@Slf4j
@Component
public class UrlFetcher {
    public Document fetchDocument(@NonNull String urlToProcess) {
        log.debug("Getting document for {}", urlToProcess);
        try {
            Connection.Response response = Jsoup.connect(urlToProcess)
                    .method(Connection.Method.GET)
                    .userAgent("Mozilla")
                    .ignoreHttpErrors(true)
                    .execute();
            if (response.statusCode() != HttpStatus.OK.value()) {
                throw new RuntimeException(String.format("Provided url cannot be analyzed. Error code %d: %s",
                        response.statusCode(), response.statusMessage()));
            }

            return response.parse();

        } catch (UnknownHostException e) {
            throw new RuntimeException(
                    String.format("Provided url cannot be analyzed. Unknown host: %s", urlToProcess));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
