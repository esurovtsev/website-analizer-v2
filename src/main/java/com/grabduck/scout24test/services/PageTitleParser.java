package com.grabduck.scout24test.services;

import com.grabduck.scout24test.models.AnalyzeResult;
import lombok.NonNull;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

@Component
public class PageTitleParser implements ParseProcessor {
    @Override
    public void parse(@NonNull Document source, @NonNull AnalyzeResult.AnalyzeResultBuilder resultBuilder) {
        resultBuilder.pageTitle(source.title());
    }
}
