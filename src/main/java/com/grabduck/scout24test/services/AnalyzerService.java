package com.grabduck.scout24test.services;

import com.grabduck.scout24test.models.AnalyzeResult;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URL;
import java.util.List;

@Slf4j
@Service
public class AnalyzerService {
    @Autowired
    private UrlFetcher urlFetcher;
    @Autowired
    private List<ParseProcessor> processors;

    public AnalyzeResult assembleWebsiteData(@NonNull URL urlToProcess) {
        Document document = urlFetcher.fetchDocument(urlToProcess.toString());
        AnalyzeResult.AnalyzeResultBuilder builder = AnalyzeResult.builder();
        processors.forEach(processor -> processor.parse(document, builder));
        return builder.build();
    }
}
