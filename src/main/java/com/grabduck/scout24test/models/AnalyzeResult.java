package com.grabduck.scout24test.models;

import lombok.Builder;
import lombok.Data;

/**
 * Not using primitives here so it is possible to provide nulls to frontend which means value was not available for
 * some reasons (most probably because of failures)
 */
@Data
@Builder
public class AnalyzeResult {
    private final String htmlVersion;
    private final String pageTitle;
    private final Integer h1;
    private final Integer h2;
    private final Integer h3;
    private final Integer h4;
    private final Integer h5;
    private final Integer h6;
    private final Integer internalLinks;
    private final Integer externalLinks;
    private final Integer inaccessibleLinks;
    private final boolean containsLoginForm;
}
