package com.grabduck.scout24test.models;

public enum UrlType {
    INTERNAL, EXTERNAL, INACCESSIBLE
}
