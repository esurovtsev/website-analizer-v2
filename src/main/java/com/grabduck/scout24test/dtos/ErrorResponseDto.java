package com.grabduck.scout24test.dtos;

import lombok.Data;

@Data
public class ErrorResponseDto {
    private String message;
}
