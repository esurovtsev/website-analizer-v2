package com.grabduck.scout24test.dtos;

import lombok.Data;

@Data
public class UrlDto {
    private String url;
}
