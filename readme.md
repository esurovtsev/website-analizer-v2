# Web-Application for Analyzing Web-Sites for ImmobilienScout24

## Description

This is a solution for ImmobilienScout24 development test. The solution is built mostly based on Spring Framework with
Spring Boot as a tool for creating stand-alone, production grade Spring based Application. For frontend part AngularJS
is taken as a fast prototyping tool.

Keeping in mind that the task ImmobilienScout24 is offering stays the same over the time and author sucessefully passed
this test 3 years ago already, a decision was made to reuse some parts of the existed code but change it with help of
modern approaches and new thoughts author may have.

Restrictions: Having certain time frame for doing the test some restrictions were applied to the final result, namely

* not the whole code was test covered, only critical places where author expected something to go wrong, were covered by tests

* no separate Spring profiles were created to split development and production environments.

* having that the perpose of the test was to demonstrate author's abilities to work with spring and java, all related
to frontend parts were kept in "simple is better" state, without any special organizing and optimization.

## How to build and run

To build the solution type `mvn clean package`, the resulting artefact can be found in target/scout24Test-0.1-SNAPSHOT.jar

To run the solution type `java -jar target/scout24Test-0.1-SNAPSHOT.jar`, a server will start and the solution will be
available on `http://localhost:8080`